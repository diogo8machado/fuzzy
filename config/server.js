const express = require('express');
const consign = require('consign');
const bodyParser = require('body-parser');
const dados = require('../app/controller/dados')
const path = require('path');
const cors = require('cors')

const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors())
console.log(dados)
app.post('/', function(req,res){
  console.log(req.body)
  const valor=dados.dados(req.body);
  res.send({valor})
});


consign()
    .then('app/controller')
    .into(app);


app.use(express.static(__dirname+'/home'));
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname+'/home','index.html'));
});
    

module.exports = app;