module.exports.dados = function(body){
    let dado = body;
    console.log(dado + " dados")

    //caso precise de vertor pertinenciaBaixaTemp.push(1)
    let pertinenciaBaixaTemp
    let pertinenciaMediaTemp
    let pertinenciaAltaTemp

    let pertinenciaBaixaUmi
    let pertinenciaMediaUmi
    let pertinenciaAltaUmi
    let calc
    let soma
    
//Fuzzificação da temperatura    
    //calculando a pertinencia para uma temperatura baixa
    if(dado.temperatura >=0 && dado.temperatura <= 16){
        if(dado.temperatura == 0){  
            pertinenciaBaixaTemp= (1)
            pertinenciaMediaTemp= (0)
            pertinenciaAltaTemp= (0)
            console.log(pertinenciaBaixaTemp+ " 2")
        }else if(dado.temperatura >= 1 && dado.temperatura <= 13){
            pertinenciaBaixaTemp= (((16-dado.temperatura)/(16)))
            pertinenciaMediaTemp= (0)
            pertinenciaAltaTemp= (0)
            console.log(pertinenciaBaixaTemp+ " 3")
        }else if(dado.temperatura >=14 && dado.temperatura <= 16){
            pertinenciaBaixaTemp= (((16-dado.temperatura)/(16)))
            pertinenciaAltaTemp= (0)
            console.log(pertinenciaBaixaTemp)
            
        }
    }

    //calculando a pertinencia para uma temperatura media
    if(dado.temperatura >=14 && dado.temperatura <= 29){
        if(dado.temperatura >= 14 && dado.temperatura <=19 ){
            pertinenciaMediaTemp= ((dado.temperatura-14)/(19-14))
            pertinenciaAltaTemp= (0)
            console.log(pertinenciaMediaTemp + " 1")
        }else if(dado.temperatura >= 20 && dado.temperatura <= 22){  
            pertinenciaBaixaTemp= (0)
            pertinenciaMediaTemp= (1)
            pertinenciaAltaTemp= (0)
            console.log(pertinenciaMediaTemp + " 2")
        }else if(dado.temperatura >= 23 && dado.temperatura <= 25 ){  
            pertinenciaBaixaTemp= (0)
            pertinenciaMediaTemp= ((29-dado.temperatura)/(29-14))
            pertinenciaAltaTemp= (0)
            console.log(pertinenciaMediaTemp + " 3")
        }else if(dado.temperatura >=26 && dado.temperatura <= 29){
            pertinenciaBaixaTemp= (0)
            pertinenciaMediaTemp= ((29-dado.temperatura)/(29-14))
            console.log(pertinenciaMediaTemp + " 4")
        }
    }
    //calculando a pertinencia para uma temperatura alta
    if(dado.temperatura >=26 && dado.temperatura <= 40){
        if(dado.temperatura >= 26 && dado.temperatura <=29 ){
            pertinenciaAltaTemp= ((dado.temperatura-26)/(40-26))
            console.log(pertinenciaAltaTemp + " 1")
        }else if(dado.temperatura >= 30 && dado.temperatura <= 40){  
            pertinenciaBaixaTemp= (0)
            pertinenciaMediaTemp= (0)
            pertinenciaAltaTemp= ((dado.temperatura-26)/(40-26))
            console.log(pertinenciaAltaTemp + " 2")
        }
    }

    //Fuzzificação da Umidade    
    //calculando a pertinencia para uma umidade baixa
    if(dado.umidade >=0 && dado.umidade <= 36){
        if(dado.umidade == 0){  
            pertinenciaBaixaUmi= (1)
            pertinenciaMediaUmi= (0)
            pertinenciaAltaUmi= (0)
            console.log(pertinenciaBaixaUmi+ " 2")
        }else if(dado.umidade >= 1 && dado.umidade <= 27){
            pertinenciaBaixaUmi= (((36-dado.umidade)/(36)))
            pertinenciaMediaUmi= (0)
            pertinenciaAltaUmi= (0)
            console.log(pertinenciaBaixaUmi+ " 3")
        }else if(dado.umidade >=30 && dado.umidade <= 36){
            pertinenciaBaixaUmi= (((36-dado.umidade)/(36)))
            pertinenciaAltaUmi= (0)
            console.log(pertinenciaBaixaUmi)
            
        }
    }

    //calculando a pertinencia para uma umidade media
    if(dado.umidade >=30 && dado.umidade <=70){
        if(dado.umidade >= 30 && dado.umidade <=47 ){
            pertinenciaMediaUmi= ((dado.umidade-30)/(70-30))
            pertinenciaAltaUmi= (0)
            console.log(pertinenciaMediaUmi + " 1")
        }else if(dado.umidade >= 48 && dado.umidade <= 55){  
            pertinenciaBaixaUmi= (0)
            pertinenciaMediaUmi= (1)
            pertinenciaAltaUmi= (0)
            console.log(pertinenciaMediaUmi + " 2")
        }else if(dado.umidade >= 56 && dado.umidade <= 64 ){  
            pertinenciaBaixaUmi= (0)
            pertinenciaMediaUmi= ((29-dado.umidade)/(70-30))
            pertinenciaAltaUmi= (0)
            console.log(pertinenciaMediaUmi + " 3")
        }else if(dado.umidade >=64 && dado.umidade <= 70){
            pertinenciaBaixaUmi= (0)
            pertinenciaMediaUmi= ((29-dado.umidade)/(70-30))
            console.log(pertinenciaMediaUmi + " 4")
        }
    }
    //calculando a pertinencia para uma umidade alta
    if(dado.umidade >=64 && dado.umidade <= 100){
        if(dado.umidade >= 26 && dado.umidade <=29 ){
            pertinenciaAltaUmi= ((dado.umidade-26)/(100-64))
            console.log(pertinenciaAltaUmi + " 1")
        }else if(dado.umidade >= 30 && dado.umidade <= 100){  
            pertinenciaBaixaUmi= (0)
            pertinenciaMediaUmi= (0)
            pertinenciaAltaUmi= ((dado.umidade-64)/(100-64))
            console.log(pertinenciaAltaUmi + " 2")
        }
    }

    
    
    if((pertinenciaBaixaTemp > pertinenciaMediaTemp && pertinenciaBaixaTemp  > pertinenciaAltaTemp) && (pertinenciaBaixaUmi >pertinenciaMediaUmi && pertinenciaBaixaUmi > pertinenciaAltaUmi)){
        calc= calculaMinimo(pertinenciaBaixaTemp, pertinenciaBaixaUmi)
        soma = calculaSoma(26,40,calc)
        console.log("def " + calc + " " +soma)

    }else if((pertinenciaBaixaTemp > pertinenciaMediaTemp && pertinenciaBaixaTemp  > pertinenciaAltaTemp) && (pertinenciaMediaUmi >pertinenciaBaixaUmi && pertinenciaMediaUmi > pertinenciaAltaUmi)){
        calc= calculaMinimo(pertinenciaBaixaTemp, pertinenciaMediaUmi)
        soma = calculaSoma(26,40,calc)
        console.log("def " + calc + " " +soma)

    }else if((pertinenciaBaixaTemp > pertinenciaMediaTemp && pertinenciaBaixaTemp  > pertinenciaAltaTemp) && (pertinenciaAltaUmi >pertinenciaBaixaUmi && pertinenciaAltaUmi > pertinenciaMediaUmi)){
        calc= calculaMinimo(pertinenciaBaixaTemp, pertinenciaAltaUmi)
        soma = calculaSoma(26,40,calc)
        console.log("def " + calc + " " +soma)

    }else if((pertinenciaMediaTemp > pertinenciaBaixaTemp && pertinenciaMediaTemp  > pertinenciaAltaTemp) && (pertinenciaBaixaUmi >pertinenciaMediaUmi && pertinenciaBaixaUmi > pertinenciaAltaUmi)){
        calc= calculaMinimo(pertinenciaMediaTemp, pertinenciaBaixaUmi)
        soma = calculaSoma(14,29,calc)
        console.log("def " + calc + " " +soma)

    }else if((pertinenciaMediaTemp > pertinenciaBaixaTemp && pertinenciaMediaTemp  > pertinenciaAltaTemp) && (pertinenciaMediaUmi >pertinenciaBaixaUmi && pertinenciaMediaUmi > pertinenciaAltaUmi)){
        calc= calculaMinimo(pertinenciaMediaTemp, pertinenciaMediaUmi)
        soma = calculaSoma(14,29,calc)
        console.log("def " + calc + " " +soma)

    }else if((pertinenciaMediaTemp > pertinenciaBaixaTemp && pertinenciaMediaTemp  > pertinenciaAltaTemp) && (pertinenciaAltaUmi >pertinenciaBaixaUmi && pertinenciaAltaUmi > pertinenciaMediaUmi)){
        calc= calculaMinimo(pertinenciaMediaTemp, pertinenciaAltaUmi)
        soma = calculaSoma(14,29,calc)
        console.log("def " + calc + " " +soma)

    }else if((pertinenciaAltaTemp > pertinenciaBaixaTemp && pertinenciaAltaTemp  > pertinenciaMediaTemp) && (pertinenciaBaixaUmi >pertinenciaMediaUmi && pertinenciaBaixaUmi > pertinenciaAltaUmi)){
        calc= calculaMinimo(pertinenciaAltaTemp, pertinenciaBaixaUmi)
        soma = calculaSoma(0,16,calc)
        console.log("def " + calc + " " +soma)

    }else if((pertinenciaAltaTemp > pertinenciaBaixaTemp && pertinenciaAltaTemp  > pertinenciaMediaTemp) && (pertinenciaMediaUmi >pertinenciaBaixaUmi && pertinenciaMediaUmi > pertinenciaAltaUmi)){
        calc= calculaMinimo(pertinenciaAltaTemp, pertinenciaMediaUmi)
        soma = calculaSoma(0,16,calc)
        console.log("def " + calc + " " +soma)

    }else if((pertinenciaAltaTemp > pertinenciaBaixaTemp && pertinenciaAltaTemp  > pertinenciaMediaTemp) && (pertinenciaAltaUmi >pertinenciaBaixaUmi && pertinenciaAltaUmi > pertinenciaMediaUmi)){
        calc= calculaMinimo(pertinenciaAltaTemp, pertinenciaAltaUmi)
        soma = calculaSoma(0,16,calc)
        console.log("def " + calc + " " +soma)

    }
    console.log("teste"+soma)
    return soma


//console.log( pertinenciaBaixaTemp,pertinenciaMediaTemp, pertinenciaAltaTemp, calc)
//console.log( pertinenciaBaixaUmi,pertinenciaMediaUmi, pertinenciaAltaUmi)
}


calculaMinimo=(valor1 , valor2)=>{
    if(valor1 <= valor2){
        return valor1
    }else{
        return valor2
    }

}

calculaSoma=(minimo, maximo, pertinencia) =>{
    let somatorio=0
    let somaPer=0
    for (i = minimo; i<maximo+1; i++ ){
        somatorio += i
        somaPer += pertinencia
        /*console.log("somatorio" +somatorio)
        console.log("somatorioPert" +somaPer)*/
    }
    return (somatorio*pertinencia)/somaPer
}
